<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="keywords" content="Private Transport and Car Hire HTML Template" />
    <meta name="description" content="Private Transport and Car Hire HTML Template">
    <meta name="author" content="themeenergy.com">

    <title>Transfers - Blog post</title>

    <link rel="stylesheet" href="/css/theme-pink.css" />
    <link rel="stylesheet" href="/css/style.css" />
    <link rel="stylesheet" href="/css/animate.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:400,500,600,700|Montserrat:400,700">
    <link rel="shortcut icon" href="/images/favicon.ico">

    <link rel="stylesheet" href="/js/mentions/recommended-styles.css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Header -->
<header class="header" role="banner">
    <div class="wrap">
        <!-- Logo -->
        <div class="logo">
            <a href="index.html" title="Transfers"><img src="/images/transfers.jpg" alt="Transfers" /></a>
        </div>
        <!-- //Logo -->

        <!-- Main Nav -->
        <nav role="navigation" class="main-nav">
            <ul>
                <li><a href="index.html" title="">Home</a></li>
                <li><a href="destinations.html" title="Destinations">Destinations</a>
                    <ul>
                        <li><a href="destination-single.html" title="Single destination">Single destination</a></li>
                        <li><a href="destination-micro.html" title="Micro destination">Micro destination</a></li>
                    </ul>
                </li>
                <li><a href="tailor-made.html" title="Tailor made">Tailor made</a></li>
                <li class="active"><a href="blog.html" title="Blog">Blog</a>
                    <ul>
                        <li><a href="blog.html" title="Post">Blog list</a></li>
                        <li><a href="blog2.html" title="Post">Blog grid</a></li>
                        <li><a href="blog-single.html" title="Post">Post</a></li>
                    </ul>
                </li>
                <li><a href="contact.html" title="Contact">Contact</a></li>
                <li><a href="#" title="Pages">Pages</a>
                    <div>
                        <div class="one-fourth">
                            <h2>Common</h2>
                            <ul>
                                <li><a href="left-sidebar-page.html" title="Left sidebar page">Left sidebar page</a></li>
                                <li><a href="right-sidebar-page.html" title="Right sidebar page">Right sidebar page</a></li>
                                <li><a href="both-sidebar-page.html" title="Both sidebars page">Both sidebars page</a></li>
                                <li><a href="full-width-page.html" title="Full width page">Full width page</a></li>
                            </ul>
                        </div>
                        <div class="one-fourth">
                            <h2>Booking</h2>
                            <ul>
                                <li><a href="search-results.html" title="Search results page">Search results page</a></li>
                                <li><a href="booking-step1.html" title="Booking step 1">Booking step 1</a></li>
                                <li><a href="booking-step2.html" title="Booking step 2">Booking step 2</a></li>
                                <li><a href="booking-step3.html" title="Booking step 3">Booking step 3</a></li>
                            </ul>
                        </div>
                        <div class="one-fourth">
                            <h2>Corporate</h2>
                            <ul>
                                <li><a href="about.html" title="About u">About us</a></li>
                                <li><a href="services.html" title="Services">Services</a></li>
                                <li><a href="faq.html" title="Faq">Faq</a></li>
                                <li><a href="contact.html" title="Contact">Contact</a></li>
                            </ul>
                        </div>
                        <div class="one-fourth">
                            <h2>Special</h2>
                            <ul>
                                <li><a href="login.html" title="Login">Login</a></li>
                                <li><a href="register.html" title="Register">Register</a></li>
                                <li><a href="error.html" title="404 error">404 error</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="/logout" title="logout">Logout</a></li>
            </ul>
        </nav>
        <!-- //Main Nav -->
    </div>
</header>
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
<!-- Page info -->
<header class="site-title color">
    <div class="wrap">
        <div class="container">
            <h1>Blog</h1>
            <nav role="navigation" class="breadcrumbs">
                <ul>
                    <li><a href="index.html" title="Home">Home</a></li>
                    <li><a href="blog.html" title="Blog">Blog</a></li>
                    <li>Post</li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!-- //Page info -->


<div class="wrap">
    <div class="row">
        <!--- Content -->
        <div class="three-fourth content">
            <!-- Post -->
            <article class="single hentry">
                <div class="entry-featured">
                    <img src="http://placehold.it/1024x768" alt="" />
                    <div class="overlay">
                        <a href="blog-single.html" class="expand">+</a>
                    </div>
                </div>
                <div class="entry-content">
                    <h2>{{$blog->title}}</h2>
                    {{$blog->story}}
                </div>
            </article>
            <!-- //Post -->
            @if(Auth::check())
            <!--respond-->
            <form id="commentForm">
                <div class="comment-respond box" id="respond">
                    <h3>Leave a reply</h3>
                    <div class="f-row">
                        <div class="full-width">
                            <label for="comments">Your comment</label>
                            <textarea id="comments" class="mentions" placeholder="You can mention those users who have commented on this blog by using @username. However the look and feel of the mentions dropdown is no very good because there is a css class clash"></textarea>
                            <input type="hidden" value="{{$blog->id}}" id="blogId" />
                        </div>
                    </div>
                    <div class="f-row">
                        <!--<input type="button" value="Submit" id="commentButton" class="btn color medium right" />-->
                        <button id="commentButton" type="button" class="btn color medium right">Submit</button>
                    </div>

                </div>
            </form>
            <!--//respond-->
            @endif
            <!--comments-->
            <div class="comments">
                <h3>{{$counter}} comments </h3>
                <ol class="comment-list">
                    <div id="extraComments"></div>
                    @foreach($comments as $comment)
                    <!--comment-->
                    <li class="comment depth-1">
                        <div class="avatar"><a href="#"><img src="/alpha/<?php echo substr($comment->username, 0,1);?>.png" alt="" /></a></div>
                        <div class="comment-box">
                            <div class="comment-author meta">
                               <strong>{{$comment->username}}</strong> said <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() ?>
                            </div>
                            <div class="comment-text">
                                <p>{{$comment->comment}}</p>
                            </div>
                        </div>
                    </li>
                    </ol>
                @endforeach
                </div>

                    <!--//comment-->

        </div>
        <!--- //Content -->

        <!--- Sidebar -->
        <aside class="one-fourth sidebar right">
            <!-- Widget -->
            <div class="widget">
                <ul class="categories">
                    <li><a href="#">Blog categories</a></li>
                    <li class="active"><a href="#">Lorem ipsum</a></li>
                    <li><a href="#">Category name</a></li>
                    <li><a href="#">Lorem ipsum</a></li>
                    <li><a href="#">Dolor</a></li>
                    <li><a href="#">Sit amet</a></li>
                </ul>
            </div>
            <!-- //Widget -->

            <!-- Widget -->
            <div class="widget">
                <h4>Advertisment</h4>
                <a href="#"><img src="http://placehold.it/800x600" alt="" /></a>
            </div>
            <!-- //Widget -->

            <!-- Widget -->
            <div class="widget">
                <h4>Need help booking?</h4>
                <div class="textwidget">
                    <p>Call our customer services team on the number below to speak to one of our advisors who will help you with all of your needs.</p>
                    <p class="contact-data"><span class="ico phone black"></span> +1 555 555 555</p>
                </div>
            </div>
            <!-- //Widget -->

            <!-- Widget -->
            <div class="widget">
                <h4>Why book with us?</h4>
                <div class="textwidget">
                    <h5>Reliable and Safe</h5>
                    <p>Lorem ipsum dolor sit amet,  do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
                    <h5>No hidden fees</h5>
                    <p>Lorem ipsum dolor sit amet,  do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
                    <h5>We’re Always Here</h5>
                    <p>Lorem ipsum dolor sit amet,  do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
                </div>
            </div>
            <!-- //Widget -->
        </aside>
        <!--- //Sidebar -->
    </div>
</div>
</main>
<!-- //Main -->

<!-- Footer -->
<footer class="footer black" role="contentinfo">
    <div class="wrap">
        <div class="row">
            <!-- Column -->
            <article class="one-half">
                <h6>About us</h6>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
            </article>
            <!-- //Column -->

            <!-- Column -->
            <article class="one-fourth">
                <h6>Need help?</h6>
                <p>Contact us via phone or email:</p>
                <p class="contact-data"><span class="ico phone"></span> +1 555 555 555</p>
                <p class="contact-data"><span class="ico email"></span> <a href="mailto:help@transfers.com">help@transfers.com</a></p>
            </article>
            <!-- //Column -->

            <!-- Column -->
            <article class="one-fourth">
                <h6>Follow us</h6>
                <ul class="social">
                    <li class="facebook"><a href="#" title="facebook">facebook</a></li>
                    <li class="twitter"><a href="#" title="twitter">twitter</a></li>
                    <li class="gplus"><a href="#" title="gplus">google plus</a></li>
                    <li class="linkedin"><a href="#" title="linkedin">linkedin</a></li>
                    <li class="vimeo"><a href="#" title="vimeo">vimeo</a></li>
                    <li class="pinterest"><a href="#" title="pinterest">pinterest</a></li>
                </ul>
            </article>
            <!-- //Column -->
        </div>

        <div class="copy">
            <p>Copyright 2014, Themeenergy. All rights reserved. </p>

            <nav role="navigation" class="foot-nav">
                <ul>
                    <li><a href="#" title="Home">Home</a></li>
                    <li><a href="#" title="Blog">Blog</a></li>
                    <li><a href="#" title="About us">About us</a></li>
                    <li><a href="#" title="Contact us">Contact us</a></li>
                    <li><a href="#" title="Terms of use">Terms of use</a></li>
                    <li><a href="#" title="Help">Help</a></li>
                    <li><a href="#" title="For partners">For partners</a></li>
                </ul>
            </nav>
        </div>
    </div>
</footer>
<!-- //Footer -->

<!-- Preloader -->
<div class="preloader">
    <div id="followingBallsG">
        <div id="followingBallsG_1" class="followingBallsG"></div>
        <div id="followingBallsG_2" class="followingBallsG"></div>
        <div id="followingBallsG_3" class="followingBallsG"></div>
        <div id="followingBallsG_4" class="followingBallsG"></div>
    </div>
</div>
<!-- //Preloader -->

<!-- jQuery -->
<script src="/js/jquery.min.js"></script>
<script src="/js/jquery.uniform.min.js"></script>
<script src="/js/jquery.slicknav.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/function.js"></script>
<script src="/js/mentions/bootstrap-typeahead.js"></script>
<script src="/js/mentions/mention.js"></script>

<script type='text/javascript'>(function(e){e.fn.extend({mention:function(t){this.opts={users:[],delimiter:"@",sensitive:true,queryBy:["name","username"],typeaheadOpts:{}};var n=e.extend({},this.opts,t),r=function(){if(typeof e=="undefined"){throw new Error("jQuery is Required")}else{if(typeof e.fn.typeahead=="undefined"){throw new Error("Typeahead is Required")}}return true},i=function(e,t){var r;for(r=t;r>=0;r--){if(e[r]==n.delimiter){break}}return e.substring(r,t)},s=function(e){var t;for(t in n.queryBy){if(e[n.queryBy[t]]){var r=e[n.queryBy[t]].toLowerCase(),i=this.query.toLowerCase().match(new RegExp(n.delimiter+"\\w+","g")),s;if(!!i){for(s=0;s<i.length;s++){var o=i[s].substring(1).toLowerCase(),u=new RegExp(n.delimiter+r,"g"),a=this.query.toLowerCase().match(u);if(r.indexOf(o)!=-1&&a===null){return true}}}}}},o=function(e){var t=this.query,r=this.$element[0].selectionStart,i;for(i=r;i>=0;i--){if(t[i]==n.delimiter){break}}var s=t.substring(i,r),o=t.substring(0,i),u=t.substring(r),t=o+n.delimiter+e+u;this.tempQuery=t;return t},u=function(e){if(e.length&&n.sensitive){var t=i(this.query,this.$element[0].selectionStart).substring(1),r,s=e.length,o={highest:[],high:[],med:[],low:[]},u=[];if(t.length==1){for(r=0;r<s;r++){var a=e[r];if(a.username[0]==t){o.highest.push(a)}else if(a.username[0].toLowerCase()==t.toLowerCase()){o.high.push(a)}else if(a.username.indexOf(t)!=-1){o.med.push(a)}else{o.low.push(a)}}for(r in o){var f;for(f in o[r]){u.push(o[r][f])}}return u}}return e},a=function(t){var r=this;t=e(t).map(function(t,i){t=e(r.options.item).attr("data-value",i.username);var s=e("<div />");if(i.image){s.append('<img class="mention_image" src="'+i.image+'">')}if(i.name){s.append('<b class="mention_name">'+i.name+"</b>")}if(i.username){s.append('<span class="mention_username"> '+n.delimiter+i.username+"</span>")}t.find("a").html(r.highlighter(s.html()));return t[0]});t.first().addClass("active");this.$menu.html(t);return this};e.fn.typeahead.Constructor.prototype.render=a;return this.each(function(){var t=e(this);if(r()){t.typeahead(e.extend({source:n.users,matcher:s,updater:o,sorter:u},n.typeaheadOpts))}})}})})(jQuery)</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#comments").mention({
                                   delimiter: '@',
                                users: {{$commentators}}
                            });

    });
</script>
</body>
</html>