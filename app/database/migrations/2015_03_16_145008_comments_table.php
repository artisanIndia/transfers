<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments',function($table)
        {
            $table->increments('id');
            $table->integer('blogid')->length(10)->unsigned();;
            $table->string('username',64);
            $table->text('comment');
            $table->timestamps();
            $table->foreign('blogid')->references('id')->on('blogs');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
