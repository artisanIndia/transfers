<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** Route to show login page */
Route::get('/','IndexController@index');

/** Route to process login */
Route::post('/','IndexController@doLogin');

/** Route to show registration page */
Route::get('register','IndexController@register');

/** Route to process registration */
Route::post('register','IndexController@doRegister');

/** Route to log the user out */
Route::get('logout','IndexController@logout');

/** Route for blog list */
Route::get('blog-list','BlogController@index');

/** Route for a single blog */
Route::get('blog/{id}/{title}','BlogController@blogPost');

/** Route to post a new comment */
Route::post('submitComment','IndexController@submitComment');

Route::get('installer','IndexController@installer');

Route::post('installer','IndexController@doInstall');

Route::get('check',function()
{
   $data = array(
       array('username' => 'khanof89',
       array('username' => 'something'),
       array('username' => 'something else'),
       array('username' => 'someoem')
                 ));

    return $data;
});