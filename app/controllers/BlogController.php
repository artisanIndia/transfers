<?php

class BlogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

      /*  $installed = Config::get('settings.installed');
        if($installed == 'no')
        {
            return Redirect::to('installer');
        }*/
        $blog = Blog::paginate(10);
        return View::make('bloglist')->with('blogs',$blog);
	}

    public function blogPost($id)
    {
        $blog = Blog::find($id);

        /** count coments for this blog post  */

        $comments = Comment::where('blogid','=', $id)->get();
        $counter = count($comments);

        $fetched = DB::table('comments')
                     ->select('comments.id', 'comments.created_at', 'blogid', 'comment','username')
                     ->join('blogs', 'comments.blogid', '=', 'blogs.id')
                     ->where('comments.blogid', '=', $id)
                     ->get();

        $commentators = Comment::where('blogid',$id)->get();
        $commentators = json_encode($commentators);
        return View::make('blogpost')->with([
                                                'blog' => $blog,
                                                'counter' => $counter,
                                                'comments' => $fetched,
                                                'commentators' => $commentators
                                            ]
        );
    }
}
