<?php

class IndexController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $installed = file_get_contents(app_path().'/installer.php');
        if($installed == 'no')
        {
            return Redirect::to('/installer');
        }

        if(Auth::check())
        {
            return Redirect::to('blog-list');
        }
		return View::make('index');
	}

    public function doLogin()
    {
        $input = Input::all();
        $rules = array(
            'username' => 'required|min:6',
            'password' => 'required:min:6'
        );
        $validator = Validator::make($input,$rules);
        if($validator->passes())
        {
            if (Auth::attempt(array('username' => $input['username'], 'password' => $input['password'])))
            {
                return Redirect::intended('blog-list');
            }
            else
            {
                return Redirect::back()->withInput(Input::except('password'));
            }
        }
        else
        {
            return Redirect::back()->withErrors($validator);
        }
    }

    public function register()
    {
        return View::make('register');
    }

    public function doRegister()
    {
        $input = Input::all();
        $rules = [
            'username' => 'required|min:6',
            'password' => 'required|min:6',
            'email'    => 'required|email'
        ];

        $validator = Validator::make($input,$rules);
        if($validator->passes())
        {
            $doRegister = new User;
            $doRegister->username = $input['username'];
            $doRegister->password = Hash::make($input['password']);
            $doRegister->email = $input['email'];
            $doRegister->save();

            return Redirect::to('/')->with('message','Registration Successful');
        }
        else
        {
            return Redirect::back()->withErrors($validator);
        }
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return Redirect::to('/')
                       ->with('message', 'You are now logged out');
    }

    public function submitComment()
    {
        $comment = Input::get('comment');
        $blogid = Input::get('blogId');

        $postComment = new Comment;
        $postComment->blogid = $blogid;
        $postComment->username = Auth::user()->username;
        $postComment->comment = $comment;
        $postComment->save();

        echo json_encode(['username' => $postComment->username, 'comment' => $postComment->comment, 'created_at' => $postComment->created_at]);
    }

    public function installer()
    {
        return View::make('installer');
    }

    public function doInstall()
    {
        $input = Input::all();

        $data = '
        <?php
        [
        $hostname => '.$input["hostname"].',
        $username => '.$input["username"].',
        $password => '.$input["password"].',
        $database => '.$input["database"].'
        ]';

        $put = file_put_contents(app_path().'/installer.php',$data);
        if($put)
        {
        Artisan::call('migrate');
        Artisan::call('db:seed');
        }

    }
}

