/**
 * Created by shahrukh on 16/03/15.
 */

$('#commentButton').click(function() {
	console.log('button clicked');
	comment = document.getElementById('comments').value;
	blogId = document.getElementById('blogId').value;

	$.post('/submitComment', {'comment': comment, blogId: blogId}, function(data) {

		var parsed = JSON.parse(data);
		var name = parsed.username;
		var letter = name.substring(0,1);
		console.log('letter is ' + letter);
		var html = '<li class="comment depth-1"><div class="avatar"><a href="#"><img src="/alpha/' + letter +'.png" alt="" /></a></div><div class="comment-box"><div class="comment-author meta"><strong>' + parsed.username + '</strong> said' + parsed.created_at.date+'</div><div class="comment-text"><p>' + parsed.comment + '</p></div></div></li>';

		$('#extraComments').append(html);
	}).success(function() {
		$('#comments').val('');
		$('#commentSuccess').html('Submitted successfully!').show().delay(5000).fadeOut();
	}).fail(function() {
		$('#commentFailed').html('Failed!').show().delay(5000).fadeOut();
	});

});

