## How to install

* Change the database settings under app/config/settings.php
* Set permission of app/storage folder to recursive 777
* On the root folder run the following command
	* php artisan migrate
	* php artisan db:seed

### You should have PHP version 5.4 or higher with mcrypt mode enabled